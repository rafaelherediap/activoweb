# -*- coding: utf-8 -*-
from flask import Flask, request
from models.product import Product
from models.venta import Detalle_Venta

app = Flask(__name__)


@app.route("/")
def index():
    return "Welcome to ActivoWeb!"


@app.route("/activoweb/search/<int:product_id>")
def search_product(product_id):
    product_model = Product({})
    product_data = product_model.browse(product_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/activoweb/list_product")
def list_product():
    product_model = Product({})
    product_data = product_model.list_product()
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/activoweb/searchbymeasure/<int:measure_id>")
def search_productbymeasure(measure_id):
    product_model = Product({})
    product_data = product_model.browsebymeasure(measure_id)
    return str(product_data)


@app.route("/activoweb/create", methods=['POST'])
def create_product():
    product_model = Product(request.json)
    product_model.create_product()

    return str('Se creo de forma exitoso el producto')


@app.route("/activoweb/modificar/<int:id>", methods=['PUT'])
def modificar_product(id):
    product_model = Product(request.json)
    product_model.modificar_product(id)

    return str('Se modifico de forma exitosa el producto')


@app.route("/activoweb/eliminar/<int:id>", methods=['DELETE'])
def eliminar_product(id):
    product_model = Product({})
    product_model.delete_product(id)

    return str('Se elimino de forma exitosa el producto')


@app.route("/activoweb/create_detalle", methods=['POST'])
def create_detalle():
    detalle_model = Detalle_Venta(request.json)
    detalle_model.create_detalle_de_venta()

    return str('Se creo de forma exitosamente la venta')


@app.route("/activoweb/modificar_detalle/<int:id>", methods=['PUT'])
def modificar_detalle(id):
    detalle_model = Detalle_Venta(request.json)
    detalle_model.modificar_detalle(id)
    return str('Se modifico de forma exitosa la venta')


@app.route("/activoweb/eliminar_detalle/<int:id>", methods=['DELETE'])
def eliminar_detalle(id):
    detalle_model = Detalle_Venta({})
    detalle_model.delete_detalle(id)

    return str('Se elimino de forma exitosa la venta')


@app.route("/activoweb/searchbyproduct/<int:product_id>", methods=['GET'])
def search_detalle(product_id):
    detalle_model = Detalle_Venta({})
    detalle_data = detalle_model.browse_detalle(product_id)
    return str(detalle_data)




if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1364, debug=True)
