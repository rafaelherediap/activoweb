create table measure(
    id serial primary key,
    name character varying,
    code character varying
);
create table product(
    id serial primary key,
    name character varying,
    code character varying,
    measure_id integer,
    foreign key (measure_id) REFERENCES measure(id)
);

create table detalle_de_venta(
    id serial primary key,
    producto_id integer,
    measure_id integer,
    cantidad integer,
    precio integer,
    monto integer,
    foreign key (producto_id) REFERENCES product(id),
    foreign key (measure_id) REFERENCES measure(id)
);


insert into measure(name, code) VALUES ('caja','m-001');
insert into measure(name, code) VALUES ('unidad','m-002');
insert into product(name, code,measure_id) VALUES ('cafe','p-001',2);
insert into product(name, code,measure_id) VALUES ('te','p-002',2);
insert into product(name, code,measure_id) VALUES ('chocolate','p-003',2);
insert into product(name, code,measure_id) VALUES ('manzanilla','p-004',1);
insert into product(name, code,measure_id) VALUES ('coca cola','p-005',1);
insert into product(name, code,measure_id) VALUES ('agua','p-006',1);
insert into product(name, code,measure_id) VALUES ('leche','p-007',2);



select * from product;
--select * from measure;
--drop table measure;
select * from detalle_de_venta;
