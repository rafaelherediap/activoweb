# -*- coding: utf-8 -*-
from database import Database


class Detalle_Venta:
    def __init__(self, detalle_venta):
        self._id = detalle_venta.get('id', False)
        self._producto_id = detalle_venta.get('producto_id', False)
        self._measure_id = detalle_venta.get('measure_id', False)
        self._cantidad = detalle_venta.get('cantidad', False)
        self._precio = detalle_venta.get('precio', False)
        self._monto = detalle_venta.get('monto', False)


    @staticmethod
    def browse_detalle(producto_id):
        browse_detalle_query = """select * from detalle_de_venta where producto_id = {producto_id}""".format(producto_id=producto_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_detalle_query)
        detalle = ps_cursor.fetchone()
        return detalle

    @staticmethod
    def list_detalle():
        browse_detalle_query = """select * from detalle_de_venta"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_detalle_query)
        detalle = ps_cursor.fetchall()
        return detalle

    def create_detalle_de_venta(self):
        create_detalle_query = """
               insert into detalle_de_venta(producto_id, measure_id, cantidad, precio, monto) VALUES ('{producto_id}','{measure_id}','{cantidad}','{precio}','{monto}')
           """.format(producto_id=self._producto_id, measure_id=self.measure_id, cantidad=self.cantidad, precio=self.precio, monto=self.cantidad*self.precio)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(create_detalle_query)
        ps_connection.commit()
        ps_cursor.close()
        ps_connection.close()

    def modificar_detalle(self, id):
        update_detalle_query = """
            update detalle_de_venta set producto_id='{producto_id}',measure_id='{measure_id}',cantidad='{cantidad}',precio='{precio}',monto='{monto}' where id={id}
        """.format(producto_id=self._producto_id, measure_id=self.measure_id, cantidad=self.cantidad, precio=self.precio, monto=self.cantidad*self.precio, id=id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(update_detalle_query)
        ps_connection.commit()
        ps_cursor.close()
        ps_connection.close()

    def delete_detalle(self, id):
        delete_detalle_query = """
               delete from detalle_de_venta where id={id}
           """.format(id=id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(delete_detalle_query)
        ps_connection.commit()
        ps_cursor.close()
        ps_connection.close()


    @property
    def producto_id(self):
        return self._producto_id

    @property
    def measure_id(self):
        return self._measure_id

    @property
    def cantidad(self):
        return self._cantidad

    @property
    def precio(self):
        return self._precio

    @property
    def monto(self):
        return self._monto

    @producto_id.setter
    def producto_id(self, producto_id):
        self._producto_id = producto_id

    @measure_id.setter
    def measure_id(self, measure_id):
        self._measure_id = measure_id

    @cantidad.setter
    def cantidad(self, cantidad):
        self._cantidad = cantidad

    @precio.setter
    def precio(self, precio):
        self._precio = precio

    @monto.setter
    def monto(self, monto):
        self._monto = monto

    def __repr__(self):
        return "Detalle_Venta" % self._id
